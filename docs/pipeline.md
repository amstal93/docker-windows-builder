```mermaid
graph TB

  SubGraph1 --> SubGraph1Flow
  subgraph "Mirrors"
  SubGraph1Flow(GerTMW)
  SubGraph1Flow -- Backup --> Zips --> Check[Deployment Checks] --> Failed1Flow[Failed]
  SubGraph1Flow -- Fallback & Mirror --> Versionlists --> Check[Deployment Checks] --> SubGraphPassed[Passed]
  end

  subgraph "Pipeline Failed"
  Failed1Flow(Failed)
  Failed1Flow -- Report --> Mail
  Failed1Flow -- Report --> Discord2[Discord]
  end

  subgraph "Pipeline Passed"
  SubGraphPassed(Passed)
  SubGraphPassed -- report --> Discord1[Discord]
  end

  subgraph "Gitlab CI"
  Node0[Env Test]  --> Node1
  Node1[Lint] --> Node2[Build]
  Node2 --> Node3[Test] --> SubGraph1[Deploy] --> Failed1Flow[Failed]
  end
```